package com.triPcups.funny.prankmaster.ui.fake_ui

import android.graphics.Color
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.messaging.FirebaseMessaging


class FakeViewModel : ViewModel() {

    private val database = FirebaseDatabase.getInstance()
    private val myRef = database.getReference("yael_token")

    val loadingStatus = MutableLiveData<Pair<String, Int>>()


    init {
        registerTokenId()
    }

    private fun registerTokenId() {
        loadingStatus.postValue(Pair("בודק..", Color.YELLOW))

        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("wow", "Fetching FCM registration token failed", task.exception)
                    loadingStatus.postValue(Pair("3שגיאה", Color.rgb(165, 42, 42)))
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                val token: String? = task.result

                // Log and save
                Log.d("wow", "new token is: $token")
                saveMyToken(token.toString())
            })
    }

    fun saveMyToken(token: String) {
        loadingStatus.postValue(Pair("שומר", Color.rgb(255, 140, 0)))
        myRef.setValue(token).addOnCompleteListener {
            if (it.isSuccessful) {
                loadingStatus.postValue(Pair("כן", Color.GREEN))
            } else {
                loadingStatus.postValue(Pair("לא", Color.RED))
            }
        }.addOnCanceledListener {
            loadingStatus.postValue(Pair("2שגיאה", Color.rgb(165, 42, 42)))
        }.addOnFailureListener {
            loadingStatus.postValue(Pair("שגיאה", Color.rgb(165, 42, 42)))
        }
    }
}