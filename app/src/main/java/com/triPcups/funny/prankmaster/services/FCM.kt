package com.triPcups.funny.prankmaster.services

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.triPcups.funny.prankmaster.R
import com.triPcups.funny.prankmaster.common.NotificationsHelper
import com.triPcups.funny.prankmaster.models.MyNotification
import com.triPcups.funny.prankmaster.models.PrankDetails
import com.triPcups.funny.prankmaster.models.PushNotificationModel
import com.triPcups.funny.prankmaster.ui.rehilut.SirenActivity


class FCM  : FirebaseMessagingService() {


    private val database = FirebaseDatabase.getInstance()
    private val myRef = database.getReference("yael_token")

    init {
        FirebaseMessaging.getInstance().subscribeToTopic("רכילות alert")
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)

        myRef.setValue(token)
    }

    override fun onMessageSent(p0: String) {
        super.onMessageSent(p0)

        Log.d("wow", "onMessageSent: $p0")
    }

//    override fun handleIntent(intent: Intent?) {
//        super.handleIntent(intent)
//
//        val title = intent?.getStringExtra("title").toString()
//        val body =intent?.getStringExtra("body").toString()
//        val phoneNumber = intent?.getStringExtra("phoneNumber").toString()
//        val textToSendInWhatsApp =intent?.getStringExtra("msgInWhatsApp").toString()
//        val alertTitle =intent?.getStringExtra("alertTitle").toString()
//        val alert2ndTitle =intent?.getStringExtra("alert2ndTitle").toString()
//
//        val incomingPrank = PushNotificationModel(
//            MyNotification(title, body),
//            PrankDetails(
//                alertTitle = alertTitle,
//                alert2ndTitle = alert2ndTitle,
//                phoneNumber = phoneNumber,
//                msgInWhatsApp = textToSendInWhatsApp
//            )
//        )
//
//        val intent = Intent(this, SirenActivity::class.java)
//        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//        intent.putExtra("incomingPrank", incomingPrank)
//        startActivity(intent)
//
//        NotificationsHelper.sendNotification(this, title, body, incomingPrank)
//    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        if (remoteMessage.notification?.title?.startsWith("yael:") == true) {
            super.onMessageReceived(remoteMessage)
            NotificationsHelper.onMessageReceived(this, remoteMessage)
        }
    }

//    @SuppressLint("StringFormatInvalid")
//    fun test(context: Context) {
//        FirebaseMessaging.getInstance().token
//            .addOnCompleteListener(OnCompleteListener { task ->
//                if (!task.isSuccessful) {
//                    Log.w("wow", "Fetching FCM registration token failed", task.exception)
//                    return@OnCompleteListener
//                }
//
//                // Get new FCM registration token
//                val token: String? = task.result
//
//                // Log and toast
//                val msg: String = token.toString()
//                Log.d("wow", token.toString())
//                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
//            })
//    }
//
//    @SuppressLint("StringFormatInvalid")
//    fun test2(context: Context) {
//        FirebaseInstanceId.getInstance().instanceId
//            .addOnCompleteListener(OnCompleteListener { task ->
//                if (!task.isSuccessful) {
//                    Log.w(
//                        "wow", "getInstanceId failed",
//                        task.exception
//                    )
//                    return@OnCompleteListener
//                }
//
//                // Get new Instance ID token
//                val token = task.result?.token
//
//                val msg: String = token.toString()
//                Log.d("wow", msg)
//                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
//            })
//
//        FirebaseMessaging.getInstance().subscribeToTopic(getString(R.string.default_notification_channel_id))
//
//    }
}