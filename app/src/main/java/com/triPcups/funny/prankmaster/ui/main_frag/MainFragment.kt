package com.triPcups.funny.prankmaster.ui.main_frag

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.triPcups.funny.prankmaster.R
import com.triPcups.funny.prankmaster.common.sendWhatsAppMsg
import com.triPcups.funny.prankmaster.databinding.MainFragmentBinding
import com.triPcups.funny.prankmaster.fcm.ApiUtils
import com.triPcups.funny.prankmaster.local_db.Constants
import com.triPcups.funny.prankmaster.models.MyNotification
import com.triPcups.funny.prankmaster.models.PrankDetails
import com.triPcups.funny.prankmaster.models.PushNotificationModel
import com.triPcups.funny.prankmaster.ui.rehilut.SirenActivity
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback

class MainFragment : Fragment(/* R.layout.main_fragment <- not necessary*/) {

    companion object {
        fun newInstance() = MainFragment()
    }

    private var currentToken: String? = null
    private lateinit var viewModel: MainViewModel
    private lateinit var binding: MainFragmentBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        initObservers()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUi()
    }

    private fun initUi() {
        binding.pushBtn.setOnClickListener {
            val phoneNumber = if( binding.phoneNumEditText.text.isNullOrEmpty()) {
                ""
            } else {
                binding.phoneNumEditText.text.toString()
            }
            val msg = if( binding.waMsgEditText.text.isNullOrEmpty()) {
                " "
            } else {
                binding.waMsgEditText.text.toString()
            }


            val firstTitle = if(binding.alarmFirstTitleEditText.text.isNullOrEmpty()) {
                ""
            } else {
                binding.alarmFirstTitleEditText.text.toString()
            }
            val secondTitle = if( binding.alarmSecondTitleEditText.text.isNullOrEmpty()) {
                ""
            } else {
                binding.alarmSecondTitleEditText.text.toString()
            }


            val notificationTitle = if( binding.notificationTitleEditText.text.isNullOrEmpty()) {
                "אייל המלך היה כאן"
            } else {
                binding.notificationTitleEditText.text.toString()
            }
            val notificationText = if( binding.notificationTextEditText.text.isNullOrEmpty()) {
                "אזעקת רכילות הופעלה"
            } else {
                binding.notificationTextEditText.text.toString()
            }

            ApiUtils.apiService.pushNotification(body = PushNotificationModel(
                MyNotification(/*TODO remove this*/"yael:" + notificationTitle, notificationText),
                PrankDetails(alertTitle = firstTitle, alert2ndTitle = secondTitle, phoneNumber = phoneNumber,msgInWhatsApp = msg),
                to=currentToken ?: Constants.MY_DEVICE_DEFAULT_FB_TOKEN

            ))?.enqueue(
                object : retrofit2.Callback<ResponseBody?> {
                    override fun onResponse(
                        call: Call<ResponseBody?>,
                        response: Response<ResponseBody?>
                    ) {
                        if (!response.isSuccessful) {
                            Log.e("wow", "onResponse: unsuccessful $response && ${response.errorBody()?.string()}")
                            return
                        }
                        response.body()?.let { incomingPrank ->
//                            incomingPrank.title
//                            incomingPrank.body
//                            incomingPrank.userId
//                            incomingPrank.id
//
                            Log.e("wow", "onResponse: $incomingPrank")
//                            Log.e("wow", "onResponse: ${incomingPrank.title}")
//                            Log.e("wow", "onResponse: ${incomingPrank.body}")
//                            Log.e("wow", "onResponse: ${incomingPrank.userId}")
//                            Log.e("wow", "onResponse: ${incomingPrank.id}")

                            return
                        }
                        Log.e("wow", "onResponse: unsuccessful error 2: $response")
                    }

                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                        Log.e("wow", "onFailure: $t")
                    }

                })
        }
    }

    private fun initObservers() {
        viewModel.currentToken.observe(viewLifecycleOwner, Observer {
            currentToken -> handleCurrentToken(currentToken) })

    }

    private fun handleCurrentToken(currentToken: String?) {
        if(currentToken?.isEmpty() == false) {
            this.currentToken = currentToken
        }
    }

}