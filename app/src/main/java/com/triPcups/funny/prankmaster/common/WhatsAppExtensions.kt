package com.triPcups.funny.prankmaster.common

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.telephony.PhoneNumberUtils
import androidx.core.content.ContextCompat


fun Context.sendWhatsAppMsg(phoneNumber: String, msg: String = "") {
    if (whatsappInstalledOrNot(this)) {
        val uri = Uri.parse("smsto:$phoneNumber")
        val sendIntent = Intent(Intent.ACTION_SENDTO, uri)
        sendIntent.putExtra(Intent.EXTRA_TEXT, msg)
        sendIntent.setPackage("com.whatsapp")
        startActivity(sendIntent)

//        val sendIntent = Intent(Intent.ACTION_SEND).apply {
//            data = Uri.parse("smsto:$phoneNumber")
//            type = "text/plain"
//            setPackage("com.whatsapp")
//            putExtra(Intent.EXTRA_TEXT, msg)
//        }
//        startActivity(sendIntent)

//        val uri = Uri.parse("whatsapp://send?phone=+91$phoneNumber&text=$msg")
//        val i = Intent(Intent.ACTION_VIEW, uri)
//        startActivity(i)

//        val sendIntent = Intent("android.intent.action.MAIN")
//        sendIntent.component = ComponentName("com.whatsapp", "com.whatsapp.Conversation")
//        sendIntent.putExtra(
//            "jid",
//            PhoneNumberUtils.stripSeparators(
//                phoneNumber.replace("+", "").replace("-", "").replace(
//                    " ",
//                    ""
//                ).toString()
//            ) + "@s.whatsapp.net"
//        ) //phone number without "+" prefix
//        startActivity(sendIntent)

//        val sendIntent = Intent("android.intent.action.MAIN")
//        sendIntent.putExtra("jid", phoneNumber.replace("+", "").replace("-", "").replace(" ", "").toString() + "@s.whatsapp.net")
//        sendIntent.putExtra(Intent.EXTRA_TEXT, message)
//        sendIntent.action = Intent.ACTION_SEND
//        sendIntent.setPackage("com.whatsapp")
//        sendIntent.type = "text/plain"
//        startActivity(sendIntent)

//        val i = Intent(
//            Intent.ACTION_SENDTO,
//            Uri.parse("content://com.android.contacts/data/$phoneNumber")
//        )
//        i.type = "text/plain"
//        i.setPackage("com.whatsapp") // so that only Whatsapp reacts and not the chooser
//        i.putExtra(Intent.EXTRA_TEXT, msg)
//        startActivity(i)
    } else {
        startActivity(
            Intent(
                Intent.ACTION_VIEW, Uri.parse(
                    String.format(
                        "https://api.whatsapp.com/send?phone=%s&text=%s",
                        phoneNumber,
                        msg
                    )
                )
            )
        )
    }
}

private fun whatsappInstalledOrNot(context: Context): Boolean {
    return try {
        context.packageManager.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES)
        true
    } catch (e: PackageManager.NameNotFoundException) {
        false
    }
}
