package com.triPcups.funny.prankmaster.fcm

import com.triPcups.funny.prankmaster.local_db.Constants
import com.triPcups.funny.prankmaster.models.PushNotificationModel
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface ApiService {



//    @Headers("Content-Type: application/json",
//        "Authorization: key=${Constants.SERVER_KEY}")
    @POST("fcm/send")
//    @FormUrlEncoded
    fun pushNotification(@Body body: PushNotificationModel?/*, @Field("body") body: String? = data.toString()*/): Call<ResponseBody?>?
//                         @Header("Authorization") authorization: String = "key=${Constants.SERVER_KEY}",
//    @Header("Content-Type") contentType:String ="application/json",

}