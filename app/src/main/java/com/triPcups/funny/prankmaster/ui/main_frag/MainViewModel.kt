package com.triPcups.funny.prankmaster.ui.main_frag

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


class MainViewModel : ViewModel() {


    private val database = FirebaseDatabase.getInstance()
    private val myRef = database.getReference("yael_token")

    var currentToken = MutableLiveData<String?>()

    init {
        readTokenFromServer()
    }

    fun readTokenFromServer() {
        // Read from the database
        // Read from the database
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                val value = dataSnapshot.getValue(String::class.java)
                Log.d("wow", "Value is: $value")
                currentToken.postValue(value)
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w("wow", "Failed to read value.", error.toException())
            }
        })
    }

}