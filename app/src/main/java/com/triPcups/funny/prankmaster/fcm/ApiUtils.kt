package com.triPcups.funny.prankmaster.fcm

import com.triPcups.funny.prankmaster.fcm.RetrofitClient.getClient

object ApiUtils {
    const val BASE_URL = "https://fcm.googleapis.com/"

    val apiService: ApiService
        get() = getClient(BASE_URL)!!.create(ApiService::class.java)
}