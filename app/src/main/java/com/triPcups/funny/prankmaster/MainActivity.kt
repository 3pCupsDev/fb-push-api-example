package com.triPcups.funny.prankmaster

import android.os.Bundle
import androidx.annotation.NonNull
import com.triPcups.funny.prankmaster.common.sendWhatsAppMsg
import com.triPcups.funny.prankmaster.databinding.ActivityMainBinding
import com.triPcups.funny.prankmaster.local_db.Constants
import com.triPcups.funny.prankmaster.services.FCM
import com.triPcups.funny.prankmaster.ui.BasicActivity
import com.triPcups.funny.prankmaster.ui.main_frag.MainFragment

class MainActivity : BasicActivity(R.id.mainActContainer) {

    override val binding: ActivityMainBinding
        get() = super.binding as @NonNull ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        showMainFragment()
        initUi()
    }

    private fun showMainFragment() {
        showFragment(MainFragment.newInstance())
    }

    private fun initUi() {

    }

}