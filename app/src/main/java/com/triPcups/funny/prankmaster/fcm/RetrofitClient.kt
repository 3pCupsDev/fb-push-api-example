package com.triPcups.funny.prankmaster.fcm

import android.util.Log
import com.google.gson.GsonBuilder
import com.triPcups.funny.prankmaster.local_db.Constants
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitClient {
    private var retrofit: Retrofit? = null

    var gson = GsonBuilder()
        .setLenient()
        .create()

    fun getClient(baseUrl: String?): Retrofit? {
        if (retrofit == null) {

            val httpClient = OkHttpClient.Builder().addNetworkInterceptor { chain ->
                val request: Request =
                    chain.request()
                        .newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Authorization", "key=" + Constants.SERVER_KEY)
                        .build()
                chain.proceed(request)
            }


            retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        }
        return retrofit
    }


//    fun getHeader(authorizationValue: String?): OkHttpClient? {
//        return OkHttpClient.Builder()
//            .addNetworkInterceptor { chain ->
//                var request: Request? = null
//                if (authorizationValue != null) {
//                    Log.d("wow--Authorization-- ", authorizationValue)
//                    val original = chain.request()
//                    // Request customization: add request headers
//                    val requestBuilder = original.newBuilder()
//                        .addHeader("Content-Type", "application/json")
//                        .addHeader("Authorization", authorizationValue)
//                    request = requestBuilder.build()
//                }
//                chain.proceed(request)
//            }
//            .build()
//    }


}