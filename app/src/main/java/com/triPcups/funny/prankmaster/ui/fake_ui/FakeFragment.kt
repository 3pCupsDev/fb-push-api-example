package com.triPcups.funny.prankmaster.ui.fake_ui

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.triPcups.funny.prankmaster.R
import com.triPcups.funny.prankmaster.databinding.FakeFragmentBinding

class FakeFragment : Fragment() {

    companion object {
        fun newInstance() = FakeFragment()
    }

    private lateinit var binding: FakeFragmentBinding
    private lateinit var viewModel: FakeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FakeFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(FakeViewModel::class.java)

        initObservers()
    }

    private fun initObservers() {
        viewModel.loadingStatus.observe(viewLifecycleOwner, Observer {
            loadingStatus -> handleLoading(loadingStatus) })
    }

    private fun handleLoading(loadingStatus: Pair<String, Int>?) {
        loadingStatus?.let { status ->
            binding.connectionStatus.text = status.first
            binding.connectionStatus.setTextColor(status.second)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        initUi()
    }

    private fun initUi() {

    }


}