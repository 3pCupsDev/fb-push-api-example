package com.triPcups.funny.prankmaster.common

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.triPcups.funny.prankmaster.R
import com.triPcups.funny.prankmaster.models.MyNotification
import com.triPcups.funny.prankmaster.models.PrankDetails
import com.triPcups.funny.prankmaster.models.PushNotificationModel
import com.triPcups.funny.prankmaster.ui.rehilut.SirenActivity

class NotificationsHelper {

    companion object {
        fun onMessageReceived(context: Context, remoteMessage: RemoteMessage) {
            val title = remoteMessage.notification!!.title?.replace("yael:", "").toString()
            val body = remoteMessage.notification!!.body.toString()

            var phoneNumber = ""
            var textToSendInWhatsApp = " "
            var alertTitle = ""
            var alert2ndTitle = " "

            if (remoteMessage.data.isNotEmpty()) {
                phoneNumber = remoteMessage.data["phoneNumber"].toString()
                textToSendInWhatsApp = remoteMessage.data["msgInWhatsApp"].toString()
                alertTitle = remoteMessage.data["alertTitle"].toString()
                alert2ndTitle = remoteMessage.data["alert2ndTitle"].toString()

                val incomingPrank = PushNotificationModel(
                    MyNotification(title, body),
                    PrankDetails(
                        alertTitle = alertTitle,
                        alert2ndTitle = alert2ndTitle,
                        phoneNumber = phoneNumber,
                        msgInWhatsApp = textToSendInWhatsApp
                    )
                )


                val intent = Intent(context, SirenActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("incomingPrank", incomingPrank)
                context.startActivity(intent)


                sendNotification(context, title, body, incomingPrank)
            }
        }

        @SuppressLint("UnspecifiedImmutableFlag")
        fun sendNotification(context: Context, title: String, body: String, incomingPrank: PushNotificationModel) {
            val m = 420
            val intent = Intent(context, SirenActivity::class.java)
            intent.putExtra("incomingPrank", incomingPrank)
            val pendingIntent = PendingIntent.getActivity(
                context,
                m, intent, PendingIntent.FLAG_ONE_SHOT
            )
            val channelId = context.getString(R.string.default_notification_channel_id)
            val defaultSoundUri: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
            val notificationManager = context.getSystemService(FirebaseMessagingService.NOTIFICATION_SERVICE) as NotificationManager

// Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                    channelId,
                    channelId,
                    NotificationManager.IMPORTANCE_HIGH
                )
                notificationManager.createNotificationChannel(channel)
            }
            notificationManager.notify(m, notificationBuilder.build())

            Log.d("wow", "sendNotification: $title")
            Log.d("wow", "sendNotification: $body")
        }
    }


}