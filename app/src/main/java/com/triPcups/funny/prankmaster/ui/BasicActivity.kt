package com.triPcups.funny.prankmaster.ui

import android.os.Bundle
import android.util.Log
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.triPcups.funny.prankmaster.MainActivity
import com.triPcups.funny.prankmaster.databinding.ActivityFakeBinding
import com.triPcups.funny.prankmaster.databinding.ActivityMainBinding
import com.triPcups.funny.prankmaster.databinding.ActivitySirenBinding
import com.triPcups.funny.prankmaster.ui.fake_ui.FakeActivity
import com.triPcups.funny.prankmaster.ui.rehilut.SirenActivity

open class BasicActivity(private val activityContainer: Int? = null) : AppCompatActivity(){

    //todo add ads
    private var _binding: Any? = null

    protected open val binding: Any?
        get() = when (localClassName) {
            "ui.rehilut.SirenActivity" -> {
                _binding as @NonNull ActivitySirenBinding
            }
            SirenActivity::class.java.simpleName -> {
                _binding as @NonNull ActivityMainBinding
            }
            "ui.fake_ui.FakeActivity" -> {
                _binding as @NonNull ActivityFakeBinding
            }
            else -> {
                _binding
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //todo add try catch
        when(localClassName) {
            "ui.rehilut.SirenActivity" -> {
                Log.d("wow", "onCreate: SirenActivity")

                _binding = ActivitySirenBinding.inflate(layoutInflater)
                setContentView((_binding as @NonNull ActivitySirenBinding).root)

            }
            MainActivity::class.java.simpleName -> {
                Log.d("wow", "onCreate: MainActivity")

                _binding = ActivityMainBinding.inflate(layoutInflater)
                setContentView((_binding as @NonNull ActivityMainBinding).root)
            }
            "ui.fake_ui.FakeActivity" -> {
                Log.d("wow", "onCreate: FakeActivity")

                _binding = ActivityFakeBinding.inflate(layoutInflater)
                setContentView((_binding as @NonNull ActivityFakeBinding).root)
            }
            else -> {
                Log.e("wow", "onCreate: no data binding handle in BasicActivity for $localClassName")
            }
        }
    }

    protected fun showFragment(fragment: Fragment, tag: String = fragment.javaClass.simpleName) {
        activityContainer?.let { container ->
            supportFragmentManager.beginTransaction()
                .replace(container, fragment, tag)
                .commit()
        }
    }
}