package com.triPcups.funny.prankmaster.ui.fake_ui

import android.os.Bundle
import androidx.annotation.NonNull
import com.google.firebase.messaging.FirebaseMessaging
import com.triPcups.funny.prankmaster.R
import com.triPcups.funny.prankmaster.databinding.ActivityFakeBinding
import com.triPcups.funny.prankmaster.ui.BasicActivity

class FakeActivity : BasicActivity(R.id.fakeActContainer) {


    override val binding: ActivityFakeBinding
        get() = super.binding as @NonNull ActivityFakeBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        FirebaseMessaging.getInstance().subscribeToTopic("רכילות alert")
        showFragment(FakeFragment.newInstance())

    }
}