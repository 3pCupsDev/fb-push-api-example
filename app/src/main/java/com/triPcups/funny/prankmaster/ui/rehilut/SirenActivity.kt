package com.triPcups.funny.prankmaster.ui.rehilut

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import androidx.annotation.NonNull
import com.triPcups.funny.prankmaster.R
import com.triPcups.funny.prankmaster.common.sendWhatsAppMsg
import com.triPcups.funny.prankmaster.databinding.ActivitySirenBinding
import com.triPcups.funny.prankmaster.models.PushNotificationModel
import com.triPcups.funny.prankmaster.ui.BasicActivity


class SirenActivity : BasicActivity(R.id.sirenActContainer) {

    private var prankData: PushNotificationModel? = null
    override val binding: ActivitySirenBinding
        get() = super.binding as @NonNull ActivitySirenBinding

    private var mp: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        intent?.extras?.get("incomingPrank")?.let { incomingPrank ->
            prankData = incomingPrank as PushNotificationModel
        }

        showRehilutFragment()
    }

    override fun onStart() {
        super.onStart()
        mp = MediaPlayer.create(this, R.raw.police_alarm)
        mp?.start()
    }

    override fun onPause() {
        super.onPause()
        mp?.release()
        mp = null

        finish()
    }

    private fun showRehilutFragment() {
        val f = RehilutFullscreenFragment()
        val args = Bundle()
        args.putString("alertTitle", prankData?.data?.alertTitle)
        args.putString("alert2ndTitle", prankData?.data?.alert2ndTitle)
        f.arguments = args

        showFragment(f)
    }

    override fun onDestroy() {
        super.onDestroy()

        val phoneNum = if(!prankData?.data?.phoneNumber.isNullOrEmpty()) {
            prankData?.data?.phoneNumber.toString()
        } else {
            ""
        }
        val msg = if(!prankData?.data?.msgInWhatsApp.isNullOrEmpty()) {
            prankData?.data?.msgInWhatsApp.toString()
        } else {
            " "
        }

        if(phoneNum.isNotEmpty()) {
            sendWhatsAppMsg(phoneNum, msg)
        }
    }

}