package com.triPcups.funny.prankmaster.models

import android.provider.Settings.Global.getString
import com.triPcups.funny.prankmaster.R
import com.triPcups.funny.prankmaster.local_db.Constants
import java.io.Serializable

class PrankDetails(var title: String? = null, var body: String? = null,var alertTitle: String? = null,var alert2ndTitle: String? = null, var phoneNumber: String,var msgInWhatsApp: String) : Serializable

class MyNotification(var title: String, var body: String) : Serializable

class PushNotificationModel(var notification : MyNotification, var data : PrankDetails ? = null, val to: String =  Constants.MY_DEVICE_DEFAULT_FB_TOKEN ) : Serializable

//maybe it needs renaming