package com.triPcups.funny.prankmaster.ui.rehilut

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.triPcups.funny.prankmaster.R
import com.triPcups.funny.prankmaster.databinding.FragmentRehilutFullscreenBinding
import su.levenetc.android.textsurface.Text
import su.levenetc.android.textsurface.TextBuilder
import su.levenetc.android.textsurface.animations.*
import su.levenetc.android.textsurface.contants.Align
import su.levenetc.android.textsurface.contants.Pivot


/**
 * An example full-screen fragment that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class RehilutFullscreenFragment : Fragment() {

    private var alertTitle: String? = null
    private var alert2ndTitle: String? = null



    private val hideHandler = Handler()
    @Suppress("InlinedApi")
    private val hidePart2Runnable = Runnable {
        // Delayed removal of status and navigation bar

        // Note that some of these constants are new as of API 16 (Jelly Bean)
        // and API 19 (KitKat). It is safe to use them, as they are inlined
        // at compile-time and do nothing on earlier devices.
        val flags =
            View.SYSTEM_UI_FLAG_LOW_PROFILE or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        activity?.window?.decorView?.systemUiVisibility = flags
        (activity as? AppCompatActivity)?.supportActionBar?.hide()
    }
    private val showPart2Runnable = Runnable {
        // Delayed display of UI elements
        fullscreenContentControls?.visibility = View.VISIBLE
    }
    private var visible: Boolean = false
    private val hideRunnable = Runnable { hide() }

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private val delayHideTouchListener = View.OnTouchListener { _, _ ->
        if (AUTO_HIDE) {
            delayedHide(AUTO_HIDE_DELAY_MILLIS)
        }
        false
    }

    private var dummyButton: Button? = null
    private var fullscreenContent: View? = null
    private var fullscreenContentControls: View? = null

    private var _binding: FragmentRehilutFullscreenBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentRehilutFullscreenBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        visible = true

        dummyButton = binding.dummyButton
        fullscreenContent = binding.fullscreenContent
        fullscreenContentControls = binding.fullscreenContentControls
        // Set up the user interaction to manually show or hide the system UI.
        fullscreenContent?.setOnClickListener { toggle() }

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        dummyButton?.setOnTouchListener(delayHideTouchListener)



        initMyUI()
    }

    private fun initMyUI() {
        val animation1: Animation = AnimationUtils.loadAnimation(context, R.anim.blink_effect)
        binding.fullscreenContent.startAnimation(animation1)

        arguments?.let { args ->
            if (!args.getString("alertTitle").isNullOrEmpty()) {
                alertTitle = args.getString("alertTitle")
            }
            if (!args.getString("alert2ndTitle").isNullOrEmpty()) {
                alert2ndTitle = args.getString("alert2ndTitle")
            }
        }


        val firstTitle = if(alertTitle.isNullOrEmpty() || alertTitle == "null") {
            "רכילות"
        } else {
            alertTitle
        }
        val secondTitle = if(alert2ndTitle.isNullOrEmpty() || alert2ndTitle == "null") {
            "!!!ALERT!!!"
        } else {
            alert2ndTitle
        }

        //start text animation
        val textDaai: Text = TextBuilder
            .create(firstTitle)
//            .setPadding(0f, 0f, 0f, 15f)
            .setSize(42f)
            .setAlpha(0)
            .setColor(Color.WHITE)
            .setPosition(Align.SURFACE_CENTER).build()

        val textDaai2: Text = TextBuilder
            .create(secondTitle)
            .setSize(55f)
            .setPadding(0f, 15F, 420f, 0f)
            .setAlpha(0)
            .setColor(Color.WHITE)
            .setPosition(Align.BOTTOM_OF, textDaai)
//            .setPosition(Align.SURFACE_CENTER)
            .build()

        binding.textSurface.play(

            Sequential(
                Parallel(
                    Rotate3D.showFromSide(textDaai, 1550, Pivot.TOP),
                    Rotate3D.showFromSide(textDaai2, 1550, Pivot.BOTTOM),
                    Alpha.show(textDaai, 690), ChangeColor.to(textDaai, 690, Color.BLACK),
                    Alpha.show(textDaai2, 690), ChangeColor.to(textDaai2, 690, Color.BLACK),
                ),
                Parallel(
                    Alpha.show(textDaai, 690), ChangeColor.to(textDaai, 690, Color.WHITE),
                    Alpha.show(textDaai2, 690), ChangeColor.to(textDaai2, 690, Color.WHITE),
                ),
                Parallel(

                    Alpha.show(textDaai, 690), ChangeColor.to(textDaai, 690, Color.BLUE),
                    Alpha.show(textDaai2, 690), ChangeColor.to(textDaai2, 420, Color.RED)
                ),
                Parallel(

                    ChangeColor.fromTo(textDaai, 1590, Color.BLACK, Color.WHITE),
                    ChangeColor.fromTo(textDaai2, 1520, Color.BLACK, Color.WHITE)
                ),
                Parallel(
                    Sequential(
                        Alpha.show(textDaai, 1200),
                        Delay.duration(690),
                        Alpha.hide(textDaai, 4200)
                    ),
                    Sequential(
                        Alpha.show(textDaai2, 1200),
                        Delay.duration(690),
                        Alpha.hide(textDaai2, 4200)
                    ),
                )
            ),

            )
    }

    override fun onResume() {
        super.onResume()
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100)
    }

    override fun onPause() {
        super.onPause()
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        // Clear the systemUiVisibility flag
        activity?.window?.decorView?.systemUiVisibility = 0
        show()
    }

    override fun onDestroy() {
        super.onDestroy()
        dummyButton = null
        fullscreenContent = null
        fullscreenContentControls = null
    }

    private fun toggle() {
        if (visible) {
            hide()
        } else {
            show()
//            Toast.makeText(context, "אני יודע תודה", Toast.LENGTH_SHORT).show()
        }
    }

    private fun hide() {
        // Hide UI first
        fullscreenContentControls?.visibility = View.GONE
        visible = false

        // Schedule a runnable to remove the status and navigation bar after a delay
        hideHandler.removeCallbacks(showPart2Runnable)
        hideHandler.postDelayed(hidePart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    @Suppress("InlinedApi")
    private fun show() {
        // Show the system bar
        fullscreenContent?.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        visible = true

        // Schedule a runnable to display UI elements after a delay
        hideHandler.removeCallbacks(hidePart2Runnable)
        hideHandler.postDelayed(showPart2Runnable, UI_ANIMATION_DELAY.toLong())
        (activity as? AppCompatActivity)?.supportActionBar?.show()
    }

    /**
     * Schedules a call to hide() in [delayMillis], canceling any
     * previously scheduled calls.
     */
    private fun delayedHide(delayMillis: Int) {
        hideHandler.removeCallbacks(hideRunnable)
        hideHandler.postDelayed(hideRunnable, delayMillis.toLong())
    }

    companion object {
        /**
         * Whether or not the system UI should be auto-hidden after
         * [AUTO_HIDE_DELAY_MILLIS] milliseconds.
         */
        private const val AUTO_HIDE = true

        /**
         * If [AUTO_HIDE] is set, the number of milliseconds to wait after
         * user interaction before hiding the system UI.
         */
        private const val AUTO_HIDE_DELAY_MILLIS = 3000

        /**
         * Some older devices needs a small delay between UI widget updates
         * and a change of the status and navigation bar.
         */
        private const val UI_ANIMATION_DELAY = 300
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}